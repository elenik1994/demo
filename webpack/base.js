const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {
  mode: "development",
  target: "web",
  entry: "./src/index.js",
  devtool: "inline-source-map",
  module: {
    rules: [
      {
        test: /\.js?$/i,
        use: "babel-loader",
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [".js"],
    alias: {
      "@lib":  path.resolve("./lib")
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve("./public/index.html"),
    }),
  ],
  output: {
    filename: "bundle-[contenthash].js",
    path: path.resolve("./dist"),
  },
  stats: {
    builtAt: false,
    children: false,
    modules: false,
    colors: {
      green: "\u001b[34;1m",
    },
  },
}
