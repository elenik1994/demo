# app
FROM cypress/base:20.5.0 AS app

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install

COPY . ./

ENTRYPOINT []
CMD []

# build
FROM app AS build

ARG node_env
RUN NODE_ENV=$node_env yarn build

# nginx
FROM nginx:stable-alpine

COPY --from=build /app/dist /static
